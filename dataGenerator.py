import numpy as np
import math
import pandas as pd
'''
fig, ax = plt.subplots(1,1)

c = 1.7866166
mean, var, skew, kurt = weibull_min.stats(c, moments='mvsk')
x = np.linspace(weibull_min.ppf(0.01, c),weibull_min.ppf(0.99, c), 100)
ax.plot(x, weibull_min.pdf(x, c),'r-', lw=5, alpha=0.6, label='weibull_min pdf')
plt.show()'''
def generateSurgp(age):
	surgp = np.zeros([14,14])
	surgp[0,0] = 1
	surgp[1,0] = (age-60)/1000. + .05
	surgp[1,1] = 1-surgp[1,0]
	for i in range(2,14):
		surgp[i,1] = surgp[1,1] - i*.03
		surgp[i,0] = 1 - surgp[i,1]
	return surgp
def generateStp(age):
	stp = np.zeros([14,14]) #surveillance transition probabilities

	#Dead
	stp[0,0] = 1 #dead people stay dead

	#AAA
	stp[1,0] = (age-60)/1000.0 + .05#%chance of dying outright 
	stp[1,1] = 1 - stp[1,0] # 95% chance of living with no AAA

	for i in range(2,14):
		stp[i,0] = stp[i-1,0] + .02 #chance of dying
		stp[i,1] = .15 - i*.01 #chance of recovering with no surgery
		if(i!=13):
			stp[i,i] = np.round(math.log(14-i, 14)/3.0, 2)#.4 - i*.02
			stp[i,i+1] = 1 - stp[i,0] - stp[i,1] - stp[i,i]#c  hance of widening
		else:
			stp[i,i] = 1 - stp[i,0] - stp[i,1]
	return stp
surgp_writer = pd.ExcelWriter('surgery_tp.xlsx')
stp_writer = pd.ExcelWriter('surveillance_tp.xlsx')
states = ['Dead', 'No AAA', '<30mm', '30-35mm', '35-40mm', '40-45mm', '45-50mm', '50-55mm', '55mm-60mm', '60mm-65mm', '65mm-70mm', '70mm-75mm', '75mm-80mm','>80mm']
for i in range(0,7):
	age = 60 + i*10
	#print(age)
	#print(generateSurgp(age))
	surgp = pd.DataFrame(generateSurgp(age),index = states)
	#surgp=surgp.reindex(states)
	surgp.columns = states
	
	stp = pd.DataFrame(generateStp(age), index = states)
	#stp=stp.reindex(states)
	stp.columns = states

	surgp.to_excel(surgp_writer, 'age={}'.format(age))
	stp.to_excel(stp_writer, 'age={}'.format(age))	
surgp_writer.save()
stp_writer.save()

